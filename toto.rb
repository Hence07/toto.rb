module Lamborghini
  attr_accessor :balance_owed, :maturity_date
  def owed_by_month
    balance_owed / maturity_date
  end
end
  class CarLoan
  include Lamborghini

end

class Monsta
  include Lamborghini
end

carloan = CarLoan.new
carloan.balance_owed = 12000
carloan.maturity_date = 12

monsta = Monsta.new
monsta.balance_owed = 300
monsta.maturity_date = 10

puts carloan.owed_by_month
puts "---"
puts monsta.owed_by_month
